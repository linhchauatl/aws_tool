class Ec2Service

  class << self
    def all_regions
      client = Aws::EC2::Client.new(region: 'us-west-1')
      client.describe_regions.regions

      # list all available regions: regions = client.describe_regions.regions
      # all us regions: us_regions = client.describe_regions.regions.select { |x| x.region_name.match(/^us/) }
    end

    # region can be something like 'us-west-1'
    def instances(region)
      resource = Aws::EC2::Resource.new(region: region)
      resource.instances.to_a
    end

    def find_instance_in_region(region, instance_id)
      begin
        resource = Aws::EC2::Resource.new(region: region)
        resource.instance(instance_id)
      rescue => error
        Rails.logger.error("Error in find_instance: #{error.message}\n#{error.backtrace.join("\n")}")
      end
    end

    def terminate(region, instance_id)
      instance = find_instance(region, instance_id)
      instance.terminate if instance
    end


    def all_instances
      result = {}
      all_regions.map(&:region_name).each do |region_name|
        instances = instances(region_name)
        result.store(region_name, instances) if instances.any?
        instances.each { |x| yield x } if block_given?
      end
      result
    end

    def current_instance_id
      metadata_endpoint = 'http://169.254.169.254/latest/meta-data/'
      Net::HTTP.get( URI.parse( metadata_endpoint + 'instance-id' ) )
    rescue => error
      Rails.logger.error("Error in current_instance: #{error.message}\n#{error.backtrace.join("\n")}")
    end

    def terminate_all_instances
      current_instance = nil
      current_id = current_instance_id

      all_instances do |instance|
        begin
          (instance.id == current_id)? (current_instance = instance) : instance.terminate
        rescue => error
          Rails.logger.error("Error terminate #{instance}: #{error.message}\n#{error.backtrace.join("\n")}")
        end
      end # all_instances

      current_instance.terminate if current_instance
    end

    def current_owner_id
      client = Aws::EC2::Client.new(region: 'us-west-1')
      client.describe_instances.reservations[0].owner_id
    rescue => error
      Rails.logger.error("Error in current_owner_id: #{error.message}\n#{error.backtrace.join("\n")}")
    end

    def find_instance(instance_id)
      instances_by_regions = all_instances
      instances_by_regions.values.each do |insts|
        instance = insts.detect { |x| x.instance_id == instance_id }
        return instance if instance
      end
    end

    def snapshots(region_name, owner_id)
      resource = Aws::EC2::Resource.new(region: region_name)
      resource.snapshots(owner_ids: [owner_id]).to_a
    end

    def all_snapshots(owner_id)
      result = {}
      all_regions.map(&:region_name).each do |region_name|
        shots = snapshots(region_name, owner_id)
        result.store(region_name, shots) if shots.any?
        shots.each { |x| yield x } if block_given?
      end
      result
    end

    def delete_all_snapshots(owner_id)
      all_snapshots(owner_id) do |shot|
        begin
          shot.delete
        rescue => error
          Rails.logger.error("Error delete #{shot}: #{error.message}\n#{error.backtrace.join("\n")}")
        end
      end # all_snapshots
    end

    def images(region_name, owner_id)
      resource = Aws::EC2::Resource.new(region: region_name)
      resource.images(owners: [owner_id]).to_a
    end

    def all_images(owner_id)
      result = {}
      all_regions.map(&:region_name).each do |region_name|
        imgs = images(region_name, owner_id)
        result.store(region_name, imgs) if imgs.any?
        imgs.each { |x| yield x } if block_given?
      end
      result
    end

    def delete_all_images(owner_id)
      all_images(owner_id) do |image|
        begin
          image.deregister if image.id != 'ami-9a773efa'
        rescue => error
          Rails.logger.error("Error delete #{image}: #{error.message}\n#{error.backtrace.join("\n")}")
        end
      end # all_images
    end

  end # class << self

end

=begin

load("#{Rails.root}/lib/ec2_service.rb")

Ec2Service.all_instances { |instance| puts instance.instance_id }

ec2_client = Aws::EC2::Client.new(region: 'us-west-1')
ec2_client.describe_images({ owners: ['owner-id'], filters: [ {name: 'is-public', values: ['false']} ]  })
ec2_client.describe_images({ filters: [ {name: 'is-public', values: ['false']} ]  })


shots = resource.snapshots(owner_ids: ['318665151411']).to_a;
shots = Ec2Service.all_snapshots('318665151411') { |shot| puts shot.inspect };


images = resource.images(owners: [ Ec2Service.current_owner_id ]).to_a
=end

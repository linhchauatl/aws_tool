# root is the root of the whole tree
# parent is the direct parent of a tree node

class Tree
  attr_accessor :root, :data, :parent, :node_type, :children

  def initialize(options = {})
    self.root      = options[:root] || self
    self.data      = options[:data]
    self.parent    = options[:parent]
    self.node_type = options[:node_type] 
    self.children  = options[:children] || []
    self
  end

  def find(data)
    find_in_tree(self.root, data)
  end

  def display
    display_node(root)
  end
  
  def to_hash
    h = { data: self.root.data, node_type: :branch, children: [] }
    self.root.children.each do |child|
      sub_hash = { data: child.data, node_type: child.node_type, children: []}
      h[:children] << sub_hash
      node_to_hash(child, sub_hash) if child.node_type == :branch
    end
    h
  end
  
  def Tree.walk_tree(node)
    yield node if node.parent.nil?
    node.children.each do |child|
      yield child
      next if child.children.empty?
      walk_tree(child) do |sub_child|
        yield sub_child
      end
    end
  end

  private

  def find_in_tree(node, data)
    return node if node.data == data
    node.children.each do |child| 
      return child if child.data == data
      find_in_tree(child, data) 
    end
  end
  
  def display_node(node)
    puts node.data
    node.children.each do |child|
      puts "..#{child.data}"
      return if child.children.empty?
      display_node(child)
    end
  end

  def node_to_hash(node, h)
    return if node.parent.nil?
    node_hash = (h[:children] || []).detect { |x| x[:data] == node.data }
    node.children.each do |child|
      sub_hash = { data: child.data, node_type: child.node_type, children: []}
      h[:children] << sub_hash
      node_to_hash(child, sub_hash) if child.node_type == :branch
    end

  end

end
require_relative 'tree'
class S3Service

  class << self
    def s3(region_name)
      # region can be something similar to 'us-west-1'
      Aws::S3::Resource.new(region: region_name)
    end

    def buckets
      result = s3('us-west-1').buckets.to_a
      if block_given?
        result.each { |x| yield x }
      end
      result
    end

    def delete_all_buckets!
      buckets.each do |bucket|
        begin
          bucket.delete!
        rescue => error
          Rails.logger.error("Error delete #{bucket}: #{error.message}\n#{error.backtrace.join("\n")}")
        end
      end
    end

    def bucket_region(bucket_name)
      @@client ||= Aws::S3::Client.new(region: 'us-east-1')
      @@client.get_bucket_location(bucket: bucket_name).location_constraint
    end

    def buckets_by_regions
      result = {}
      buckets.each do |bucket|
        zone = bucket_region(bucket.name)
        buckets_in_zone = (result[zone] || []) << bucket
        result[zone] = buckets_in_zone
      end
      result
    end

    def object_list(bucket_name)
      bucket = s3(bucket_region(bucket_name)).bucket(bucket_name)

      # Array of Aws::S3::ObjectSummary
      bucket.objects.to_a
    end

    def upload(local_file_path, bucket_name, object_key)
      obj = s3(bucket_region(bucket_name)).bucket(bucket_name).object(object_key)
      obj.upload_file(local_file_path)
    end

    def download(bucket_name, object_key, local_file_path = "#{Rails.root}/tmp")
      bucket = s3(bucket_region(bucket_name)).bucket(bucket_name)
      raise "Bucket #{buckket_name} does not exist!" unless bucket.exists?

      obj = bucket.object(object_key)
      raise "File #{object_key} does not exist!" unless obj.exists?

      content = obj.get.body.read
      content_file = File.new("#{local_file_path}/#{object_key.split('/').last}", 'wb')
      content_file.write(content)
      content_file.flush
      content_file.close
      Rails.logger.debug("Write to #{local_file_path}/#{object_key.split('/').last}")
    end

    # def bucket_tree(bucket_name)
    #   bucket = @@s3.bucket(bucket_name)
    #   tree = bucket.as_tree # tree.collection.bucket.name returns the bucket name
    #   root = Tree.new({data: bucket.name, node_type: :branch})
    #   build_tree(tree, root)
    #   root.to_hash
    # end

    # private
    #
    # def build_tree(s3_node, display_node)
    #   s3_node.children.each do |child|
    #     if child.branch?
    #       display_child = Tree.new({ root: display_node.root, parent: display_node, data: child.prefix, node_type: :branch})
    #       display_node.children << display_child
    #
    #       build_tree(child, display_child)
    #     elsif child.leaf?
    #       display_child = Tree.new({ root: display_node.root, parent: display_node, data: child.key, node_type: :leaf})
    #       display_node.children << display_child
    #     end
    #
    #   end
    # end

  end #class methods

end

=begin
S3Service.buckets
S3Service.buckets_by_regions
S3Service.object_list('test-lc-s3')
S3Service.upload("#{local_path}/lib/s3_service.rb", 'test-lc-s3', 's3_service.rb')
S3Service.download('test-lc-s3', 's3_service.rb')

client.get_bucket_location(bucket: 'test-lc-s3').location_constraint
=> "us-west-1"

S3Service.buckets { |x| x.delete! }
bucket.delete!
=end
